//
//  InstafilterApp.swift
//  Instafilter
//
//  Created by Admin on 10.03.2022.
//

import SwiftUI

@main
struct InstafilterApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
