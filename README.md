# Instafilter
This in an app that lets the user import photos from their library, then modify them using various image effects.
## Used features
- importing photos from user galery
- saving result to user galery 
- using Apple’s Core Image framework
## App's overview
![screen-gif](./Instafilter/Instafilter.gif)
