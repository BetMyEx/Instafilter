//
//  ContentView.swift
//  Instafilter
//
//  Created by Admin on 10.03.2022.
//
import CoreImage
import CoreImage.CIFilterBuiltins
import SwiftUI


struct ContentView: View {
    @State private var image: Image?
    @State private var filterIntensity = 0.5
    @State private var filterRadius = 0.5

    @State private var showingImagePicker = false
    @State private var inputImage: UIImage?
    
    @State private var currentFilter: CIFilter = CIFilter.sepiaTone()
    @State private var showingFilterSheet = false
    
    @State private var processedImage: UIImage?
    let filterList = ["Crystallize":CIFilter.crystallize(),"Edges":CIFilter.edges(),"Gaussian Blur":CIFilter.gaussianBlur(),"Pixellate":CIFilter.pixellate(),"Sepia Tone":CIFilter.sepiaTone(),"Unsharp Mask":CIFilter.unsharpMask(),"Vignette":CIFilter.vignette(),"Color Invert":CIFilter.colorInvert(),"Comic Effect":CIFilter.comicEffect(),"Motion Blur":CIFilter.motionBlur()] as [String : CIFilter]

    
    let context = CIContext()
    
    var body: some View {
        NavigationView{
            VStack {
                ZStack{
                    Rectangle()
                        .fill(.secondary)
                    
                    Text("Tap to select a picture")
                        .foregroundColor(.white)
                        .font(.headline)
                    
                    image?
                        .resizable()
                        .scaledToFit()
                }
                .onTapGesture {
                    showingImagePicker = true
                }
                
                HStack{
                    Text("Intensity")
                    Slider(value: $filterIntensity)
                        .onChange(of: filterIntensity){ _ in applyProcessing()}
                 }
                .padding()
                
                HStack{
                    Text("Radius")
                    Slider(value: $filterRadius)
                        .onChange(of: filterRadius){ _ in applyProcessing()
                            print(filterRadius)
                        }
                 }
                .padding()
                
                HStack {
                    Button("Change Filter") {
                        showingFilterSheet = true
                    }
                    
                    Spacer()
                    
                    Button("Save", action: save)
                        .disabled(inputImage == nil)
                }
            }
            .padding([.horizontal, .bottom])
            .navigationTitle("Instafilter")
            .onChange(of: inputImage) { _ in loadImage() }
            .sheet(isPresented: $showingImagePicker) {
                ImagePicker(image: $inputImage)
            }
            .confirmationDialog("Select a filter", isPresented: $showingFilterSheet) {
                let keys = filterList.map{$0.key}
                let values = filterList.map{$0.value}
                ForEach(keys.indices) {index in
                    Button("\(keys[index])") {
                        setFilter(values[index])
                    }
                }
//                ForEach(filterList, id: \.self) { filter in
//                    Button("\(filter)") {
//                        switch filter {
//                        case "Crystallize":
//                            setFilter(CIFilter.crystallize())
//                        case "Edges":
//                            setFilter(CIFilter.edges())
//                        case "Gaussian Blur":
//                            setFilter(CIFilter.gaussianBlur())
//                        case "Pixellate":
//                            setFilter(CIFilter.pixellate())
//                        case "Sepia Tone":
//                            setFilter(CIFilter.sepiaTone())
//                        case "Unsharp Mask":
//                            setFilter(CIFilter.unsharpMask())
//                        case "Vignette":
//                            setFilter(CIFilter.vignette())
//                        case "Color Invert":
//                            setFilter(CIFilter.colorInvert())
//                        case "Comic Effect":
//                            setFilter(CIFilter.comicEffect())
//                        case "Motion Blur":
//                            setFilter(CIFilter.motionBlur())
//                        default:
//                            return
//                        }
//
//                    }
//                }
                Button("Cancel", role: .cancel) { }
            }
        }
    }
    func loadImage() {
        guard let inputImage = inputImage else { return }
        let beginImage = CIImage(image: inputImage)
        currentFilter.setValue(beginImage, forKey: kCIInputImageKey)
        applyProcessing()
        
    }
    
    func save() {
        guard let processedImage = processedImage else {
            return
        }
        
        let imageSaver = ImageSaver()
        
        imageSaver.successHandler = {
            print("Success!")
        }
        imageSaver.errorHandler = {
            print("Oops! \($0.localizedDescription)")
        }
        
        imageSaver.writeToPhotoAlbum(image: processedImage)
    }
    
    func applyProcessing() {
        let inputKeys = currentFilter.inputKeys
        
        if inputKeys.contains(kCIInputIntensityKey) {
            currentFilter.setValue(filterIntensity, forKey: kCIInputIntensityKey)
        }
        if inputKeys.contains(kCIInputRadiusKey) {
            currentFilter.setValue(filterRadius * 200, forKey: kCIInputRadiusKey)
        }
        if inputKeys.contains(kCIInputScaleKey) {
            currentFilter.setValue(filterIntensity * 10, forKey: kCIInputScaleKey)
        }
        
        
        guard let outputImage = currentFilter.outputImage else { return  }
        
        if let cgImage = context.createCGImage(outputImage, from: outputImage.extent) {
            let uiImage = UIImage(cgImage: cgImage)
            image = Image(uiImage: uiImage)
            processedImage = uiImage
        }
    }
    
    func setFilter(_ filter: CIFilter) {
        currentFilter = filter
        loadImage()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
